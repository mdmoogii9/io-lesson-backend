import express from "express";
//bagshiin functions
import teacher from "../controller/teacher";

let router = express.Router();
//bagshiin jagsaalt avah
router.post("/list", teacher.list);
//bagshiin medeelel uusgeh
router.post("/create", teacher.create);
//bagshiin medeeelel uurchluh
router.put("/:id", teacher.update);
//bagshiin medeelel ustgah
router.delete("/:id", teacher.delete);
//bagshiin delgerengui medeelel harah
router.get("/:id", teacher.read);

export default router;
