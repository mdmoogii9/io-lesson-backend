import express from "express";
//bagshiin functions
import lesson from "../controller/lesson";

let router = express.Router();
//hicheeliin jagsaalt avah
router.post("/list", lesson.list);
//hicheeliin medeelel uusgeh
router.post("/create", lesson.create);
//hicheeliin medeeelel uurchluh
router.put("/:id", lesson.update);
//hicheeliin medeelel ustgah
router.delete("/:id", lesson.delete);
//hicheeliin delgerengui medeelel harah
router.get("/:id", lesson.read);

export default router;
