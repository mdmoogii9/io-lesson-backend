import Teacher from "./teacherRoute";
import Student from "./studentRoute";
import Lesson from "./lessonRoute";
export { Teacher, Student, Lesson };
