import express from "express";
import student from "../controller/student";
const router = express.Router();
//systemd nevtreh
router.post("/login", student.login);
//suragchiin jagsaalt avah
router.post("/list", student.list);
//suragchiin medeelel uusgeh
router.post("/create", student.create);
//suragchiin medeeelel uurchluh
router.put("/:id", student.update);
//suragchiin medeelel ustgah
router.delete("/:id", student.delete);
//suragchiin delgerengui medeelel harah
router.get("/:id", student.read);
export default router;
