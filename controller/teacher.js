//ugudliin sangiin medeelliig undsen index.js avah
import { client } from "../index";
//bagsh narin medeellig jagsaaltaar avah
exports.list = async (req, res) => {
  try {
    const query = `select * from teacher`;
    const result = await client.query(query);
    return res.json({
      success: true,
      data: result.rows,
      total: result.rowCount,
    });
  } catch (error) {
    return res.json({ success: false, message: error });
  }
};
//bagshiin medeelel oruulah
exports.create = async (req, res) => {
  try {
    //bagshiin medeelel body-s avah
    const { teachername, phone } = req.body;
    //query uusgeh
    const query = `INSERT INTO TEACHER (teachername,phone) 
                   VALUES ('${teachername}','${phone}')`;

    console.log(
      "🚀 ~ file: teacher.js ~ line 42 ~ exports.create= ~ query",
      query
    );
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //hariuag butsaah
    return res.json({
      success: true,
      total: result.rowCount,
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
//bagshiin medeelel uurchluh
exports.update = async (req, res) => {
  try {
    //params-s bagshiin id-g avah
    const { id } = req.params;
    //body-s uurchluh medeelel avah
    const { phone } = req.body;
    //query uusgeh
    const query = `UPDATE TEACHER set phone='${phone}' where teacherid='${id}'`;
    console.log(
      "🚀 ~ file: teacher.js ~ line 61 ~ exports.update= ~ query",
      query
    );
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
      data: result.rows,
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
//bagshiin medeelel ustgah
exports.delete = async (req, res) => {
  try {
    //params-s bagshiin id-g avah
    const { id } = req.params;
    //query uusgeh
    const query = `DELETE FROM teacher where teacherid='${id}'`;
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
    });
  } catch (error) {
    return res.json({ success: false, message: error });
  }
};

exports.read = async (req, res) => {
  try {
    const { id } = req.params;
    //query uusgeh
    const query = `select * from teacher where teacherid='${id}'`;
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
      data: result.rows[0],
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
