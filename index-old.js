//service bichhed ashiglana
const express = require("express");
//post-n body-r data avhin tul ashiglana
const parser = require("body-parser");
//md5 encrypt
const md5 = require("md5");
//jwt
const { expressjwt } = require("express-jwt");
//jwt create
const jwt = require("jsonwebtoken");
//cors
const cors = require("cors");
//ugugdliin san holboh
const { Client } = require("pg");
//ugugdliin sangiin medeelel oruulah
const client = new Client({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "asdqwe",
  database: "users",
});
//ugugdliin san-tai holboh function
const dbconnect = async () => {
  await client.connect((error) => {
    error
      ? console.log(
          "ugugdliin saintai holbogdohod daraah aldaa garlaa : ",
          error
        )
      : console.log("ugugdliin saintai amjilttai holbogdloo ");
  });
};
////ugugdliin san-tai holboh
dbconnect();

const app = express();
//pareser ashiglaj body-g avah
app.use(
  parser.urlencoded({
    extended: false,
  })
);
app.use(
  cors({
    origin: ["www.iotechnology.mn", "shgdjasgd", "dasidhjasd"],
    methods: ["POST", "GET", "PUT", "DELETE"],
    allowedHeaders: ["Authorization"],
    credentials: true,
  })
);
app.use(
  parser.json({
    limit: "5mb",
  })
);
app.use(
  expressjwt({ secret: "io", algorithms: ["HS256"] }).unless({
    path: [
      {
        url: "/login",
        method: "POST",
      },
    ],
  })
);
//
app.get("/test/:id", function (request, response) {
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", request.method);
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", request.path);
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", request.params);
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", request.data);

  response.send("Hello World-2");
});

//garaas oruulsan toog - too bolgoh
app.get("/number/:too", (req, res) => {
  //params-r irsen toog huvisagchid avah
  const { too } = req.params;
  //hariu butsaah ued tootsoolol hiij hariug butsaaah
  return res.json({ hariult: too * -1 });
});
//garaas oruulsan 2toonii kvadratin niilber oloh
app.post("/number/kvadratinNiilber", (req, res) => {
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", req.params);
  console.log("🚀 ~ file: index.js ~ line 5 ~ request", req.body);
  //body-r irsen toog huvisagchid avah
  const { too1, too2 } = req.body;
  //hariu butsaah ued tootsoolol hiij hariug butsaaah
  return res.json({ hariult: too1 * too1 + too2 * too2 });
});
//buh bagshiin medeelliiig jagsaaltaar avah
app.get("/bagshiinjagsaalt", async (req, res) => {
  //query-g database deer ajiluulj ur dung avah
  let result = await client.query("select * from teacher");
  //query-ni ur dung butsaah
  return res.json({
    tuluv: "amjilttai",
    data: result.rows,
    niit: result.rowCount,
  });
});
//CRUD  --- Create Read Update Delete

//bagshiin delgerengui medeelel avah
app.get("/bagshiinmedeelel/:id", async (req, res) => {
  //params-n id-s bagshiin id-g avah
  let bagshiinId = req.params.id;
  //bagshiin medeelel avah query
  let query = `select * from teacher 
  where teacherid = '${bagshiinId}'`;
  //uussen query-g harah
  console.log("bagshiinmedeelel query", query);
  //query-g database deer ajiluulj ur dung avah
  let result = await client.query(query);
  //query-ni ur dung shalgaj hariug butsaah
  if (result.rows.length > 0)
    return res.json({
      tuluv: "amjilttai",
      data: result.rows,
    });
  else
    return res.json({
      tuluv: "medeelel oldsongui",
    });
});

//bagshiin medeelel nemeh
app.post("/bagshnemeh", async (req, res) => {
  console.log("request-body", req.body);
  //request-n body-s utguudig avah
  const { phone, teachername } = req.body;
  //insert query bichih
  let query = `INSERT INTO teacher ("teachername","phone")
   VALUES('${teachername}','${phone}')`;

  //query-g database deer ajiluulaj ur dung avah
  let result = await client.query(query);
  //hariu butsaah
  return res.json({ tuluv: result.rowCount > 0 ? "amjilltai" : "amtjilgui" });
});

//bagshiin medeelel ustgah
app.delete("/bagshustgah/:id", async (req, res) => {
  console.log("request-body", req.body);
  //request-n params-s bagshin id-g avah
  const teacherid = req.params.id;
  //ustgah query bichih
  const query = `DELETE from teacher where "teacherid" = '${teacherid}' `;
  //query ajiluulah
  const result = await client.query(query);
  //hariu butsaah
  return res.json({ tuluv: result.rowCount > 0 ? "amjilltai" : "amtjilgui" });
});

//bagshiin medeelel uurchluh
app.put("/bagshuurchluh/:id", async (req, res) => {
  console.log("request-body", req.body);
  //body-s iresen utgiih avah
  const { phone } = req.body;
  //params uurchluh bagshiin id- avah
  const teacherid = req.params.id;
  //update query bichih
  const query = `UPDATE teacher set "phone"='${phone}' WHERE id='${teacherid}'`;
  console.log("update query : ", query);
  //query ajiluulah
  const result = client.query(query);
  return res.json({ tuluv: result.rowCount > 0 ? "amjilltai" : "amtjilgui" });
});
//nevtreh
app.post("/login", async (req, res) => {
  //body-s username password avah
  const { username, password } = req.body;

  //ugugdliin sand baigaa esehiig shalgah query
  const query = `select * from student where firstname='${username}' and password='${md5(
    password
  )}'`;

  //md5 encypt check
  console.log("md5", md5(password));

  //ugudliin sand ajiluulah
  const result = await client.query(query);

  if (result.rowCount > 0) {
    const user = result.rows[0];
    const token = jwt.sign(user, "io", { expiresIn: 3600 });
    return res.json({ success: true, user: user, token: token });
  } else {
    return res.json({
      success: false,
      message: "hereglegchiin medeelel oldsongui",
    });
  }
});
//serves-g 3000 port deer ajiluulah
app.listen(3000, () => {
  console.log("server running 3000 PORT");
});
