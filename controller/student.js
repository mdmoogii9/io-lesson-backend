import md5 from "md5";
import jwt from "jsonwebtoken";
//ugudliin sangiin medeelliig undsen index.js avah
import { client } from "../index";
//suragc narin medeellig jagsaaltaar avah
exports.list = async (req, res) => {
  try {
    const query = `select * from student`;
    const result = await client.query(query);
    return res.json({
      success: true,
      data: result.rows,
      total: result.rowCount,
    });
  } catch (error) {
    return res.json({ success: false, message: error });
  }
};
//suragchiin systemd nevtreh
exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;

    const query = `select * from student 
                   where firstname='${username}' and password='${md5(
      password
    )}'`;
    console.log(
      "🚀 ~ file: suragchiin.js ~ line 31 ~ exports.login= ~ query",
      query
    );
    const result = await client.query(query);
    const token = await jwt.sign(result.rows[0], "io", { expiresIn: 600000 });
    if (result.rowCount > 0) {
      return res.json({
        success: true,
        user: result.rows[0],
        token: token,
      });
    } else {
      return res.json({
        success: true,
        message: "Suragchiin medeelel oldsongui",
      });
    }
  } catch (error) {
    return res.json({ success: false, message: error }).status(499);
  }
};
//suragchiin medeelel oruulah
exports.create = async (req, res) => {
  try {
    //suragchiin medeelel body-s avah
    const { lastname, firstname, sex, password } = req.body;
    //query uusgeh
    const query = `INSERT INTO student (lastname,firstname,sex,password) 
                     VALUES ('${lastname}','${firstname}','${sex}','${md5(
      password
    )}')`;

    console.log(
      "🚀 ~ file: teacher.js ~ line 42 ~ exports.create= ~ query",
      query
    );
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //hariuag butsaah
    return res.json({
      success: true,
      total: result.rowCount,
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
//suragchiin medeelel uurchluh
exports.update = async (req, res) => {
  try {
    //params-s bagshiin id-g avah
    const { id } = req.params;
    //body-s uurchluh medeelel avah
    const { lastname } = req.body;
    //query uusgeh
    const query = `UPDATE student set lastname='${lastname}' where studentid='${id}'`;
    console.log(
      "🚀 ~ file: teacher.js ~ line 61 ~ exports.update= ~ query",
      query
    );
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
      data: result.rows,
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
//suragchiin medeelel ustgah
exports.delete = async (req, res) => {
  try {
    //params-s bagshiin id-g avah
    const { id } = req.params;
    //query uusgeh
    const query = `DELETE FROM student where studentid='${id}'`;
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
    });
  } catch (error) {
    return res.json({ success: false, message: error });
  }
};

exports.read = async (req, res) => {
  try {
    const { id } = req.params;
    //query uusgeh
    const query = `select * from student where studentid='${id}'`;
    //query-g ugugdliin sand ajiluulah
    const result = await client.query(query);
    //ur dung butsaah
    return res.json({
      success: true,
      data: result.rows[0],
    });
  } catch (error) {
    //aldaanii medeelel butsaah
    return res.json({ success: false, message: error });
  }
};
