//service bichhed ashiglana
import express from "express";
//post-n body-r data avhin tul ashiglana

import parser from "body-parser";
//md5 encrypt
import md5 from "md5";
//jwt

import { expressjwt } from "express-jwt";
//jwt create
import jwt from "jsonwebtoken";
//cors
import cors from "cors";
//ugugdliin san holboh

//teacher route oruulah
import { Lesson, Student, Teacher } from "./routes";
//ugugdliin sangiin medeelel oruulah
//ugugdliin san holboh
import { Client } from "pg";
//ugugliin sangiin tohirgoo
const client = new Client({
  host: "localhost",
  port: 5432,
  user: "postgres",
  password: "asdqwe",
  database: "users",
});

const app = express();
//ugugdliin santai holbogdoh function
const dbconnect = async () => {
  await client.connect((error) => {
    error
      ? console.log(
          "ugugdliin saintai holbogdohod daraah aldaa garlaa : ",
          error
        )
      : console.log("ugugdliin saintai amjilttai holbogdloo ");
  });
};
//ugugdliin santai holbgdoh
dbconnect();
//pareser ashiglaj body-g avah
app.use(
  parser.urlencoded({
    extended: false,
  })
);
const allowedOrigins = ["http://localhost:3000"];

app.use(
  cors({
    origin: allowedOrigins,
    methods: ["POST", "GET", "PUT", "DELETE"],
    allowedHeaders: [
      "Authorization",
      "Content-Type",
      "Accept-Ranges",
      "Access-Control-Allow-Origin",
    ],
    credentials: true,
  })
);
app.use(
  parser.json({
    limit: "5mb",
  })
);
app.use(
  expressjwt({ secret: "io", algorithms: ["HS256"] }).unless({
    path: [
      {
        url: "/student/login",
        method: "POST",
      },
    ],
  })
);
//teacher route holboh
app.use("/teacher", Teacher);
//student route holboh
app.use("/student", Student);
//lesson route holboh
app.use("/lesson", Lesson);
//serves-g 3000 port deer ajiluulah
app.listen(3030, () => {
  console.log("server running 3030 PORT");
});
export { client };
